﻿using HiWindowsForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace HiWindowsForms
{
    public partial class FormOne : Form
    {
        private readonly RestaurantsController restaurantsController = new RestaurantsController();

        public FormOne()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            QueryRestaurants();
        }

        private void CreateButton_Click(object sender, System.EventArgs e)
        {
            var restaurant = GetRestaurantFromUI();
            restaurantsController.Create(restaurant);
            ClearUI();
            QueryRestaurants();
        }

        private void ModifyButton_Click(object sender, System.EventArgs e)
        {
            var restaurant = GetRestaurantFromUI();
            restaurantsController.Modify(restaurant);
            ClearUI();
            QueryRestaurants();
        }

        private void DeleteButton_Click(object sender, System.EventArgs e)
        {
            var restaurant = GetRestaurantFromUI();
            restaurantsController.Delete(restaurant.Id);
            ClearUI();
            QueryRestaurants();
        }

        private void QueryButton_Click(object sender, System.EventArgs e)
        {
            QueryRestaurants();
        }

        private void RestaurantsListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedItem = (sender as ListBox).SelectedItem;
            if (selectedItem != null)
            {
                var resaturant = selectedItem as Restaurant;
                IdTextBox.Text = resaturant.Id.ToString();
                NameTextBox.Text = resaturant.Name;
                AddressTextBox.Text = resaturant.Address;
                PhoneNumberTextBox.Text = resaturant.PhoneNumber;
                TagsTextBox.Text = string.Join(Environment.NewLine, resaturant.Tags);
            }
        }


        private void QueryRestaurants()
        {
            var restaurants = restaurantsController.Query(KeywordTextBox.Text);
            SetItems(restaurants);
            ClearUI();
        }

        private Restaurant GetRestaurantFromUI()
        {
            int.TryParse(IdTextBox.Text, out var id);
            var restaurant = new Restaurant
            {
                Id = id,
                Name = NameTextBox.Text,
                Address = AddressTextBox.Text,
                PhoneNumber = PhoneNumberTextBox.Text,
                Tags = TagsTextBox.Text.Split(
                    new string[] { Environment.NewLine },
                    StringSplitOptions.None
                ).ToList()
            };
            return restaurant;
        }

        private void ClearUI()
        {
            IdTextBox.Text = string.Empty;
            NameTextBox.Text = string.Empty;
            AddressTextBox.Text = string.Empty;
            PhoneNumberTextBox.Text = string.Empty;
            TagsTextBox.Text = string.Empty;
        }

        private void SetItems(List<Restaurant> restaurants)
        {
            RestaurantsListBox.Items.Clear();
            foreach (var restaurant in restaurants)
            {
                RestaurantsListBox.Items.Add(restaurant);
            }
        }
    }
}
