﻿using HiWindowsForms.Models;
using System.Collections.Generic;
using System.Linq;

namespace HiWindowsForms
{
    public class FellowsController
    {
        public List<Fellow> List(string name)
        {
            var fellows = Store.Fellows.Where(x => x.Name == name).ToList();
            return fellows;
        }

        public Fellow Detail(int id)
        {
            var fellow = Store.Fellows.FirstOrDefault(x => x.Id == id);
            return fellow;
        }

        public void Create(Fellow fellow)
        {
            Store.Fellows.Add(fellow);
        }

        public void Modify(Fellow fellow)
        {
            var target = Detail(fellow.Id);
            target.Name = fellow.Name;
        }

        public void Delete(int id)
        {
            var target = Detail(id);
            var index = Store.Fellows.IndexOf(target);
            Store.Fellows.RemoveAt(index);
        }
    }
}
