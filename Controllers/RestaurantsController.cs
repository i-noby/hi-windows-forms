﻿using HiWindowsForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HiWindowsForms
{
    public class RestaurantsController
    {
        public List<Restaurant> Query(string name)
        {
            var restaurants = Store.Restaurants.Where(x => x.Name.Contains(name)).ToList();
            return restaurants;
        }

        public Restaurant Detail(int id)
        {
            var restaurant = Store.Restaurants.FirstOrDefault(x => x.Id == id);
            return restaurant;
        }

        public void Create(Restaurant restaurant)
        {
            restaurant.Id = (int)DateTime.Now.Ticks;
            Store.Restaurants.Add(restaurant);
        }

        public void Modify(Restaurant restaurant)
        {
            var target = Detail(restaurant.Id);
            target.Name = restaurant.Name;
            target.Address = restaurant.Address;
            target.PhoneNumber = restaurant.PhoneNumber;
            target.Tags = restaurant.Tags;
        }

        public void Delete(int id)
        {
            var target = Detail(id);
            var index = Store.Restaurants.IndexOf(target);
            if (index != -1)
            {
                Store.Restaurants.RemoveAt(index);
            }
        }
    }
}
