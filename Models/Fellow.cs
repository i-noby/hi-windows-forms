﻿namespace HiWindowsForms.Models
{
    /// <summary>
    /// 小夥伴基本資訊
    /// </summary>
    public class Fellow
    {
        /// <summary>
        /// 唉低
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
    }
}
