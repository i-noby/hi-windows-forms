﻿using HiWindowsForms.Models;
using System.Collections.Generic;

namespace HiWindowsForms
{
    internal static class Store
    {
        public static List<Restaurant> Restaurants { get; set; } = new List<Restaurant>(new Restaurant[]
        {
            new Restaurant { Id = 1, Name = "好食堂", Address = "好地方", PhoneNumber = "(02) 2345-6789" },
            new Restaurant { Id = 2, Name = "深夜食堂", Address = "暗巷", PhoneNumber = "(02) 1111-2222" },
        });

        public static List<Fellow> Fellows { get; set; } = new List<Fellow>();
    }
}
